import os
import simpleguitk as simplegui
import json
import random
import urllib.request

"""
TODO:
add already_guessed list to the canvas
"""

url = urllib.request.urlopen("https://raw.githubusercontent.com/sindresorhus/mnemonic-words/master/words.json")
words = json.loads(url.read())
word = random.choice(words)
already_guessed = []
blank_list = len(word) * "*"
display_list = []
counter = 0

def counter_change(key):
    switcher = {
        2: [[275, 150], [275, 250]],
        3: [[275, 200], [310, 165]],
        4: [[275, 200], [240, 165]],
        5: [([[275, 200], [275, 248]]),
            ([[275, 248], [310, 285]]),
            ([[275, 248], [275, 248]])],
        6: [[275, 248], [240, 285]],
    }
    return switcher.get(key, [])

def new_game(sender = True):
    """
    Resets variable conditions for a new game
    """
    
    global url
    global words
    global word
    global already_guessed
    global blank_list
    global display_list
    global counter

    if(sender):
        url = urllib.request.urlopen("https://raw.githubusercontent.com/sindresorhus/mnemonic-words/master/words.json")
        words = json.loads(url.read())
        word = random.choice(words)    
        already_guessed = []
        blank_list = len(word) * "*"
        display_list = []
        counter = 0
        blank_list_label.set_text("Guess the word: " + blank_list)
    else:
        url = urllib.request.urlopen("https://raw.githubusercontent.com/sindresorhus/mnemonic-words/master/words.json")
        words = json.loads(url.read())
        word = random.choice(words)    
        already_guessed = []
        blank_list = len(word) * "*"
        display_list = []
        counter = 0
        blank_list_label.set_text("Guess the word: " + blank_list)
        response_label.set_text("")

def guess_check(guess):
    """
    Takes in a guess in the form of a string
    Manages response_label changes, current blank_list, guess counter, and already_guessed list
    """

    global already_guessed
    global blank_list
    global counter

    solved = False
    response_label.set_text("")

    # handles duplicate guesses
    if(guess in already_guessed):
        guess_input.set_text("")
        response_label.set_text("You already guessed that! Try again.")
        return 0
    else:
        already_guessed.append(guess)

    index = 0   

    # check if letter is in word
    if(guess in word):
        # iterate through string and replace letters in word and blank_list
        for i in range(len(word)):
            if(guess == word[index]):
                index_in_word = index                

                # replace letters in blank list
                blank_list = blank_list[:index_in_word] + guess + blank_list[index_in_word+1:]
                index += 1
                if(blank_list == word):
                    solved = True
            else:
                index += 1
        blank_list_label.set_text("Guess the word: " + blank_list)
        guess_input.set_text("")
        response_label.set_text("Nice job! " + guess + " was in the word!")
        if(blank_list == word):
            end_game()

    elif(solved == False):
        counter += 1
        response_label.set_text(guess + " is not in the word!")
        guess_input.set_text("")
        if(counter == 6):
            solved = True
            end_game()

def end_game(sender = True):
    """
    Manages end game effects
    """

    global counter

    if(sender):
        if(counter == 6):
            response_label.set_text("You lost! \nThe word was: " + word)
            guess_input.set_text("")
            new_game()

        else:   
            response_label.set_text("You solved it! The word was " + word +".")
            new_game()
    else:
        response_label.set_text("You lost! \nThe word was: " + word)
        guess_input.set_text("")
        new_game()

def counter_draw(canvas):
    canvas.draw_line([100, 50], [275, 50], 4, "White")
    canvas.draw_line([100, 48], [100, 350], 4, "White")
    canvas.draw_line([275, 48], [275, 100], 4, "White")
    canvas.draw_line([50, 350], [150, 350], 10, "White")
    canvas.draw_polyline(display_list, 4, "White")

    if(counter > 0):
        canvas.draw_circle([275, 125], 25, 4, "White")
        display_list.extend(counter_change(counter))

def inp_guess(guess):
    """
    Handles inputs into guess_input
    """

    try:
        guess = guess[0].lower()
        guess_check(guess)
    except:
        return

def eg_button():
    end_game(False)

def ng_button():
    new_game(False)

frame = simplegui.create_frame("Hangman", 400, 400, 200)
canvas = frame.set_draw_handler(counter_draw)
guess_input = frame.add_input("Guess something!", inp_guess, 75)
blank_list_label = frame.add_label("Guess the word: " + blank_list)
response_label = frame.add_label("")
new_game_button = frame.add_button("New game", ng_button, 50)
end_game_button = frame.add_button("End game", eg_button, 50)

frame.start()