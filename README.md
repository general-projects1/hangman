# Hangman

## Description

A simple version of hangman made with python using SimpleGUITk. 

## Releases

[Latest Release](https://gitlab.com/general-projects1/hangman/-/releases/0.3)

[Release Page](https://gitlab.com/general-projects1/hangman/-/releases/)

## Todo

1. Start menu
2. Move away from github words list dependency
3. Change graphics dependency

## Contributers

Devin Singh <drsingh2518@icloud.com>

## Dependencies used
[SimpleGUITk](https://pypi.org/project/SimpleGUITk/)


## License
[MIT](https://choosealicense.com/licenses/mit/)